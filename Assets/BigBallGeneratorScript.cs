﻿using UnityEngine;
using System.Collections;

public class BigBallGeneratorScript : MonoBehaviour {

	// Use this for initialization
    int gencounter = 0;
    public GameObject BigBall;

    public GameObject player1; public GameObject player2;
    public GameObject plane1; public GameObject plane2;

    public GUIText guit;
    public AudioClip EndGameSound;
    
    //private Vector3 swaploc;
    // This script also randomly swaps player1 and player2.
    // And also activating small energy ball generation.

	void Start () {

        int rndsign = Random.Range(0, 2) == 0 ? -1 : 1;
        if (rndsign == 1)
        {
            // Swap priority of Player1 and Player2
            GameObject p1;
            p1 = (GameObject)Instantiate(player1);
            p1.GetComponent<PlayerScript>().enabled = true;
            Destroy(player1);
        }
        else
            player1.GetComponent<PlayerScript>().enabled = true;

        StartCoroutine("Generate");
        //StartCoroutine("PlayBGMLoop");

        transform.Translate(0, 0, Mathf.Pow(Random.Range(0f, 1f), 0.3f) * rndsign * 9f);
        
        
        
	}
	
	IEnumerator Generate()
    {
        
        /*yield return new WaitForSeconds(1);
        guit.text = "3";*/
        yield return new WaitForSeconds(1);
        guit.text = "2";
        yield return new WaitForSeconds(1);
        guit.text = "1";
        yield return new WaitForSeconds(1);
        guit.text = "BEGIN!";
        yield return new WaitForSeconds(1);
        guit.text = "";

        plane1.GetComponent<PlaneScript>().enabled = true;
        plane2.GetComponent<PlaneScript>().enabled = true;

        yield return new WaitForSeconds(Random.Range(5, 15));
        while(true)
        {
            gencounter += Random.Range(-5, 25);

            if (gencounter >= 200)
            {
                //Instantiate(BigBall);
                Instantiate(BigBall, transform.position, new Quaternion());
                //bigball.transform.position.y = randomy;
                yield return new WaitForSeconds(0.5f);
                plane1.GetComponent<PlaneScript>().elapsed = true;
                plane2.GetComponent<PlaneScript>().elapsed = true;
                //Destroy(this.gameObject);
                
                Destroy(GetComponent<Light>());
                
                goto exit;
                
            }

            GetComponent<Light>().intensity = gencounter * 0.02f;
            GetComponent<Light>().range = gencounter * 0.04f;
            yield return new WaitForSeconds(0.04f);
        }
        
    exit:
        yield return null;
         
    }

    public void EndGame(bool loserisplayer2)
    {
        guit.text = "Player" + (loserisplayer2 ? "1" : "2") + "\n승리!";
        StartCoroutine("IEndgame");
    }

    IEnumerator IEndgame()
    {
        audio.Stop();
        audio.PlayOneShot(EndGameSound);
        yield return new WaitForSeconds(6);
        Application.LoadLevel("Title Scene");
    }
    
}
