﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

	// Use this for initialization
    
    


    // Update is called once per frame
    private const float r = 1.414214f;
    private const float speed = 25f;
    public bool isplayer2;
    public GameObject SmallBall;
    public GameObject BigBall;
    private KeyCode left;
    private KeyCode right;
    private KeyCode up;
    private KeyCode down;
    private KeyCode launch;
    private int maxlife = 150;
    private int life;
    private int maxenergy = 100;
    private int energy;
    private float launchcharge;
    private int launchchargeint;
    bool getBigBall = false;
    float stuntimer;
    public GameObject HealthBar;
    public GameObject EnergyBar;
    public GameObject EnergyChargeBar;
    GameObject healthbar;
    GameObject energybar;
    GameObject energychargebar;
    public AudioClip overload;
    public AudioClip bighit;
    public AudioClip smallshot;
    public AudioClip bigshot;

    void Start()
    {
        left = !isplayer2 ? KeyCode.A : KeyCode.LeftArrow;
        right = !isplayer2 ? KeyCode.D : KeyCode.RightArrow;
        up = !isplayer2 ? KeyCode.W : KeyCode.UpArrow;
        down = !isplayer2 ? KeyCode.S : KeyCode.DownArrow;
        launch = !isplayer2 ? KeyCode.G : KeyCode.Return;

        life = maxlife;

        healthbar = (GameObject)Instantiate(HealthBar);
        healthbar.transform.position = new Vector3(!isplayer2 ? -15f : 15f, healthbar.transform.position.y, healthbar.transform.position.z);
        energybar = (GameObject)Instantiate(EnergyBar);
        energybar.transform.position = new Vector3(!isplayer2 ? -15f : 15f, energybar.transform.position.y, energybar.transform.position.z);
        energychargebar = (GameObject)Instantiate(EnergyChargeBar);
        energychargebar.transform.position = new Vector3(!isplayer2 ? -15f : 15f, energychargebar.transform.position.y, energychargebar.transform.position.z);

        
    }

    void Update()
    {
        //체력바, 에너지바
        healthbar.transform.localScale = new Vector3(28f * Mathf.Clamp(life,0,maxlife) / maxlife, 0f, 1.5f);
        healthbar.transform.position = new Vector3(!isplayer2 ? -29f + 14f * life / maxlife : 29f - 14f * life / maxlife, healthbar.transform.position.y, healthbar.transform.position.z);
        energybar.transform.localScale = new Vector3(28f * Mathf.Clamp(energy,0,maxenergy) / maxenergy, 0f, 1.5f);
        energybar.transform.position = new Vector3(!isplayer2 ? -29f + 14f * energy / maxenergy : 29f - 14f * energy / maxenergy, energybar.transform.position.y, energybar.transform.position.z);
        energychargebar.transform.localScale = new Vector3(28f * Mathf.Clamp(launchcharge, 0, maxenergy) / maxenergy, 0f, 1.5f);
        energychargebar.transform.position = new Vector3(!isplayer2 ? -29f + 14f * launchcharge / maxenergy : 29f - 14f * launchcharge / maxenergy, energychargebar.transform.position.y, energychargebar.transform.position.z);

        //energybar.transform.renderer.material.color.a = stuntimer == 0 ? 0 : 0.5f;

        // Movement
        
        float x = 0;
        float y = 0;

        if (Input.GetKey(left))
        {
            x -= 1;
        }
        if (Input.GetKey(right))
        {
            x += 1;
        }
        if (Input.GetKey(up))
        {
            y += 1;
        }
        if (Input.GetKey(down))
        {
            y -= 1;
        }
        if ((x!=0)&&(y!=0))
        {
            x /= r;
            y /= r;
        }
        if (x != 0 || y != 0)
            transform.rotation = Quaternion.LookRotation(new Vector3(x, 0, y));
        x *= Time.smoothDeltaTime*speed;
        y *= Time.smoothDeltaTime*speed;


        if (!Physics.Raycast(transform.position, new Vector3(x, 0, 0), Time.smoothDeltaTime * speed + transform.localScale.x * 0.5f))
            transform.Translate(x, 0, 0, Space.World);

        if (!Physics.Raycast(transform.position, new Vector3(0, 0, y), Time.smoothDeltaTime * speed + transform.localScale.x * 0.5f))
            transform.Translate(0, 0, y, Space.World);

        // Set material color

        if (getBigBall == false)
        {
            if (energy < 255f / 5f)
            {
                gameObject.renderer.material.color = new Color((255f - energy * 5f) / 255f, (255f - energy * 5f) / 255f, 1f, 1f);
            }
            else
            {
                gameObject.renderer.material.color = Color.blue;
                //GetComponent<Light>().intensity = 1.9f;
            }
            GetComponent<Light>().intensity = Mathf.Clamp(launchcharge * 0.05f, 0, 2f);
            GetComponent<Light>().range = 3f + launchcharge * 0.06f;
            GetComponent<Light>().color = new Color(0.25f, 0.5f, 1f);
            //GetComponent<Light>().color = Color.black;
        }
        else
        {
            gameObject.renderer.material.color = Color.red;
            GetComponent<Light>().intensity = 2f;
            GetComponent<Light>().range = 6f - 2 * stuntimer;
            GetComponent<Light>().color = Color.red;
        }
        


        // Launch

        stuntimer -= Time.smoothDeltaTime;
        stuntimer = Mathf.Clamp(stuntimer, 0, 100);
        launchchargeint = (int)launchcharge;

        if (stuntimer == 0)
        {
            if (getBigBall == false)
            {
                if(Input.GetKey(launch) && launchcharge < 1 && energy >= 1)
                    launchcharge = 1;

                if (Input.GetKey(launch))
                    launchcharge += Time.smoothDeltaTime * 15;

                launchcharge = Mathf.Clamp(launchcharge, 0, energy<50 ? energy : 50);
                launchchargeint = (int)launchcharge;

                if (Input.GetKeyUp(launch) && launchchargeint > 0)
                {
                    if (launchchargeint <= 20)
                        audio.PlayOneShot(smallshot);
                    else
                        audio.PlayOneShot(bigshot);

                    GameObject smallball = (GameObject)Instantiate(SmallBall);
                    smallball.transform.position = transform.position;
                    smallball.transform.rotation = transform.rotation;
                    smallball.renderer.material.color = new Color((255f - launchchargeint * 5f) / 255f, (255f - launchchargeint * 5f) / 255f, 1f, 1f);
                    smallball.GetComponent<SmallEnergyBallScript>().Initialize(launchchargeint, isplayer2);
                    energy -= launchchargeint;

                    launchcharge = 0;

                }
            }
            else
            {
                if (Input.GetKeyUp(launch))
                {
                    GameObject bigball = (GameObject)Instantiate(BigBall);
                    audio.PlayOneShot(bigshot);
                    bigball.transform.position = transform.position;
                    bigball.transform.rotation = transform.rotation;
                    //bigball.transform.Translate(Vector3.forward * BigBall.transform.localScale.x);
                    bigball.GetComponent<BigEnergyBallScript>().Initialize(isplayer2);
                    getBigBall = false;

                }
            }
        }
	}

    void OnTriggerStay ( Collider collider) 
    {
        // Get small Energy

        if(collider.tag=="Mineral")
        {
            if (energy<maxenergy)
                energy++;

            Destroy(collider.gameObject);
        }

        // Hit by bullet

        else if (collider.tag=="Bullet")
        {
            if (collider.GetComponent<SmallEnergyBallScript>().isplayer2 ^ isplayer2)
            {
				if (collider.GetComponent<SmallEnergyBallScript>().getStrength() >= 20)
				{

					audio.PlayOneShot(bighit);
				}
                life -= collider.GetComponent<SmallEnergyBallScript>().getStrength();
                Destroy(collider.gameObject);
                //stuntimer += 0.01f;
            }
        }

        // Get big bullet / Hit by big bullet

        else if (collider.tag == "BigBullet")
        {
            launchcharge = 0;
            if (collider.GetComponent<BigEnergyBallScript>().isactive == true)
            {
                if (collider.GetComponent<BigEnergyBallScript>().isplayer2 ^ isplayer2)
                {
                    
                    life -= 30;
                    Destroy(collider.gameObject);
                    getBigBall = true;
                    stuntimer += 3;
                    audio.PlayOneShot(overload);
                }
            }
            else
            {
                if (!collider.GetComponent<BigEnergyBallScript>().ispickedup)
                {
                    Destroy(collider.gameObject);
                    getBigBall = true;
                    collider.GetComponent<BigEnergyBallScript>().ispickedup = true;
                }
            }
        }

        //DIE

        if (life <= 0)
        {
            //Some fancy death function
            //FindObjectOfType<GUIText>().text = "Player" + (isplayer2 ? "1" : "2") + "승리!";
            GetComponent<AudioSource>().enabled = false;
            FindObjectOfType<BigBallGeneratorScript>().EndGame(isplayer2);
            energy = 0;
            launchcharge = 0;
            Update();
            Destroy(gameObject);
        }
    }
  /*
    void OnGUI()
    {
        int left;

        left = !isplayer2 ? 20 : Screen.width - 150;

        GUI.Label(new Rect(left, 20, 130, 20), "플레이어 " + (!isplayer2 ? "1" : "2"));
        GUI.Label(new Rect(left, 40, 130, 20), "남은 생명: " + life);
        GUI.Label(new Rect(left, 60, 130, 20), "모은 에너지: " + energy);
        GUI.Label(new Rect(left, 80, 130, 20), "장전한 에너지: " + launchchargeint);

        if (stuntimer > 0)
            GUI.Label(new Rect(left, 100, 130, 20), "과부하: " + stuntimer);
    }
    */
}
