﻿using UnityEngine;
using System.Collections;

public class SmallEnergyBallScript : MonoBehaviour {

    private int strength;
    private float speed;
    private const float standardspeed = 1.2f;
    public bool isplayer2;
    

	// Use this for initialization
    /*
	void Start () {
	
	}
	*/
    public void Initialize (/*float speed,*/ int strength, bool isplayer2)
    {
        //this.speed = speed;
        this.strength = strength;
        transform.localScale = new Vector3(transform.localScale.x * (1f + 0.07f * strength), 0.1f, transform.localScale.z * (1f + 0.07f * strength));
        //transform.localScale = new Vector3(10, 0.1f, 10);
        this.isplayer2 = isplayer2;
        speed = standardspeed * Random.Range(0.9f, 1.1f);
    }

    public int getStrength()
    {
        return strength; 
    }

	// Update is called once per frame
    
	void Update () {
        transform.Translate(Vector3.forward * speed);
	}
     

    void OnTriggerEnter(Collider collider)
    {
        
        if (collider.tag == "Wall")
        {
            Destroy(gameObject);
        }
    }
}
