﻿using UnityEngine;
using System.Collections;

public class PlaneScript : MonoBehaviour {

	// Use this for initialization
    /*
	void Start () {
	
	}
	*/
    private int frameCounter = 0;
    public GameObject sp;
    public bool isplane2;
    public bool elapsed;

	// Update is called once per frame
	void Update () {
        frameCounter++;
        if (frameCounter > Random.Range(20, 100))
        {
            Vector3 vec = new Vector3(Mathf.Pow(Random.Range(0f, 1f), elapsed ? 0.6f : 4) * 28 - 14, 0, Random.Range(-1f, 1f) * 14);
            if (isplane2)
                vec = -vec;
            
            GameObject tmp = (GameObject)Instantiate(sp, vec+transform.position, transform.rotation);
            tmp.transform.parent = transform;

            frameCounter = 0;
        }
                
	}
}
