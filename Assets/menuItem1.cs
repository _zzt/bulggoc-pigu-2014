﻿using UnityEngine;
using System.Collections;

public class menuItem1 : MonoBehaviour {

	// Use this for initialization
	void Start () {

        this.particleSystem.Stop();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseEnter()
    {
        this.particleSystem.Play();
    }
    void OnMouseExit()
    {
        this.particleSystem.Stop();
    }
    void OnMouseUpAsButton()
    {
        Application.LoadLevel("Main Scene");
    }
}
