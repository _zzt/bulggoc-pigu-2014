﻿using UnityEngine;
using System.Collections;

public class BigEnergyBallScript : MonoBehaviour {

    private const float standardspeed = 1f;
    public bool isplayer2;
    public bool isactive = false;
    public bool ispickedup = false;

    // Use this for initialization
    /*
	void Start () {
	
	}
	*/
    public void Initialize(bool isplayer2)
    {
        isactive = true;
        this.isplayer2 = isplayer2;
    }


    void Update()
    {
        if (isactive==true)
            transform.Translate(Vector3.forward * standardspeed);
    }


    void OnTriggerEnter(Collider collider)
    {

        if (collider.tag == "Wall")
        {
            isactive = false;
        }
    }
}
